#!/bin/bash

#    NextD64 (c) 2015-2021  Žarko Živanov

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# activating speaker for beep command on Ubuntu:
# sudo modprobe pcspkr

# initial values
skipside=1
dobeep=0
d64options=""
autod64=0
mergedir=""
retrycnt=5
tracksno=35
dprefix="d"
dnumbers=3
starttrack=1
endtrack=$tracksno
startend=0
script=$(basename "$0")
ctrlc=0

# trap function for ctrl+c
function control_c()
{
    ctrlc=1
}

# read the options
TEMP=`getopt -o kor:t:p:n:m:abhs:e:x --long skip,original,retry:,tracks:,prefix:,num:,merge,auto,beep,help,start:,end:,examples -n 'nextd64.sh' -- "$@"`
ecode=$?
if [ $ecode -gt 0 ]; then
    echo "Use -h to see options"
    exit 1
fi
eval set -- "$TEMP"

# extract options and their arguments into variables.
while true ; do
    case "$1" in
        -k|--skip) skipside=2; shift ;;
        -b|--beep)
            which beep 1>/dev/null 2>/dev/null
            if [ $? -eq 0 ]; then
                dobeep=1
            else
                echo -e "\033[1;31mbeep command not found!\033[0m"
            fi
            shift ;;
        -o|--original) d64options="$d64options --transfer=original" ; shift ;;
        -a|--auto) autod64=1 ; shift ;;
        -r|--retry)
            case "$2" in
                "") shift 2 ;;
                *) retrycnt=$2 ; shift 2 ;;
            esac ;;
        -t|--tracks)
            case "$2" in
                "") shift 2 ;;
                *) tracksno=$2 ; shift 2 ;;
            esac ;;
        -p|--prefix)
            case "$2" in
                "") shift 2 ;;
                *) dprefix="$2" ; shift 2 ;;
            esac ;;
        -n|--numbers)
            case "$2" in
                "") shift 2 ;;
                *) dnumbers="$2" ; shift 2 ;;
            esac ;;
        -m|--merge)
            case "$2" in
                "") shift 2 ;;
                *) mergedir=$2 ; shift 2 ;;
            esac ;;
        -s|--start)
            case "$2" in
                "") shift 2 ;;
                *) starttrack=$2 ; startend=1 ; shift 2 ;;
            esac ;;
        -e|--end)
            case "$2" in
                "") shift 2 ;;
                *) endtrack=$2 ; startend=1 ; shift 2 ;;
            esac ;;
        -x|--examples)
            echo "Usage examples"
            echo -e "\nStart with D64 image name d123a"
            echo -e "   $script d123a"
            echo -e "\nStart with D64 image name d123, automatically determine side"
            echo -e "   $script d123"
            echo -e "\nRepeat transferring last image, only tracks 30-32"
            echo -e "   $script -s 30 -e 32"
            echo -e "\nTransfer disk with 42 tracks"
            echo -e "   $script -t 42"
            echo -e "\nMerge all dir files with standard prefix (d) into 001-042.dir"
            echo -e "   $script -m 001-042"
            exit 0 ;;
        -h|--help)
            echo "NextD64 1.2 (c) 2015-2021  Žarko Živanov"
            echo "Released under GPL v3 licence"
            echo -e "\nScript for automating d64 images creation. Auto-generated names"
            echo -e "are in the form dNNNS-Q.d64 where NNN is image number, S is side"
            echo -e "(a or b), and Q is sequence number of the same image (the format"
            echo -e "can be changed with -p and -n options). If the image is transfered"
            echo -e "without errors, sequence number is omitted. Script needs OpenCBM"
            echo -e "installed (d64copy command), PyD64Fix program (can be found on CSDB)"
            echo -e "and beep command for sounds."
            echo -e "\nUsage: $script [options] [image name]"
            echo -e "\nOptions:"
            echo -e "  -k, --skip       - skip disk side (if floppy is single-sided)"
            echo -e "  -b, --beep       - beep after copying (requires beep command installed)"
            echo -e "  -o, --original   - use original transfer mode instead of warp (d64copy option)"
            echo -e "  -r N, --retry N  - number of retries (5 by default)"
            echo -e "  -t N, --tracks N - number of tracks, 35, 40 or 42 (35 by default)"
            echo -e "  -p P, --prefix P - set another disk image prefix (d by default)"
            echo -e "  -n N, --num N    - set another number od image digits (3 by default)"
            echo -e "  -s N, --start N  - set start track for copying (1 by default), overrides -t"
            echo -e "  -e N, --end N    - set end track for copying (35 by default), overrides -t"
            echo -e "  -a, --auto       - auto-wait for next floppy (not available with -s,-e,-t)"
            echo -e "  -m, --merge DIR  - merge all dir files into one DIR file"
            echo -e "  -x, --examples   - usage examples\n"
            echo -e "  -h, --help       - this help\n"
            exit 0 ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done
if [ $retrycnt -gt 0 ]; then d64options="$d64options --retry-count=$retrycnt" ; fi
if [ $startend -eq 1 ]; then
    d64options="$d64options --start-track=$starttrack --end-track=$endtrack"
    autod64=0
else
    if [ $tracksno -gt 35 ]; then
        d64options="$d64options --end-track=$tracksno"
        autod64=0
    fi
fi

# merge dir files if merge option is specified
if [ "$mergedir" != "" ]; then
    if [ "${mergedir:(-4)}" != ".dir" ]; then mergedir="${mergedir}.dir"; fi
    echo -e "Merged dir files for mask $dprefix*.dir" > "$mergedir"
    echo -e -n "\nMerging "
    for dir in $dprefix*.dir; do
        echo -n "$dir "
        echo -e "\n\n\n$dir" >> "$mergedir"
        cat "$dir" >> "$mergedir"
    done
    echo -e "\n\nDir files merged to \033[1;32m$mergedir\033[0m\n"
    exit 0
fi

# check if PyD64Fix is present
which pyd64fix.py 1>/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
    if [ ! -f pyd64fix.py ]; then
        echo -e "\033[1;31mpyd64fix.py not found!\033[0m"
        exit 1
    fi
fi
# save keyboard stdin in descriptor 10
exec 10<&0

# make pattern for image numbers searching
dpattern=""
for ((i=0; i<$dnumbers; i++)); do dpattern="$dpattern?"; done

# reset/initialize drive
cbmctrl reset
cbmctrl command 8 "I:0"

# main loop for reading
while [ true ]; do
    lastd64=""
    #if [ "$1" != "" ] && [ $autod64 -eq 0 ]; then
    if [ "$1" != "" ]; then
        # check if specified name has some images already
        if [ -n "$(shopt -s nullglob; echo $1*.d64)" ]; then
            lastd64=$(ls $1*.d64 | tail -n 1)
        else
            if [[ "${1:(-1)}" =~ ^[0-9]$ ]]; then
                lastd64="$1a-0"
            else
                if [[ "${1:(-1)}" =~ ^[ab]$ ]]; then
                    lastd64="$1-0"
                else
                    echo -e "\033[1;31mYou cannot have side \"${1:(-1)}\".\033[0m"
                    exit 1
                fi
            fi
        fi
    else
        # check if there are auto-generated d64 files
        if [ -n "$(shopt -s nullglob; echo $dprefix$dpattern[ab]*.d64)" ]; then
            # find last auto-generated d64 file
            lastd64=$(ls -v $dprefix$dpattern[ab]*.d64 | tail -n 1)
        fi
    fi
    lenp=${#dprefix}
    n1=$((lenp+dnumbers+1))
    n2=$((lenp+dnumbers+3))
    n3=$((lenp+dnumbers))
    n4=$((lenp+dnumbers+2))
    # initial name
    if [ "$lastd64" == "" ]; then lastd64="d001a-0.d64"; fi
    # if image has no sequence number (image without errors), add one
    if [ "${lastd64:$n1:1}" != "-" ]; then lastd64="${lastd64:0:$n1}-0.d64"; fi
    # remove extension
    lastd64=${lastd64:0:$n2}
    # disk number
    lastnum=${lastd64:$lenp:$dnumbers}
    # disk side
    lastside=${lastd64:$n3:1}
    # disk sequence number
    lastseq=${lastd64:$n4:1}
    # disk directory
    lastdir="$dprefix${lastnum}${lastside}.dir"
    if [ -f "$lastdir" ]; then
        lastdir=$(cat "$lastdir")
    else
        lastdir="EMPTY"
    fi
    # debug
    #echo "lenp=$lenp# n1=$n1# n2=$n2# n3=$n3# n4=$n4# n5=$n5# lastd64=$lastd64# pattern=$dprefix$dpattern#"
    #echo "lastnum=$lastnum# lastside=$lastside# lastseq=$lastseq#"
    #echo -e "lastdir\n$lastdir"
    #exit 0

    # get directory of current floppy
    currdir=$(cbmctrl dir 8)
    # convert to decimal number (leading zeroes means octal!!!)
    lastnum=$((10#$lastnum))
    # if it is the same directory as the last one
    #if [ "$lastdir" == "$currdir" ]; then
    #    echo "SAME"
    #else
    #    echo "DIFFERENT"
    #    echo "LAST:\n##$lastdir##"
    #    echo "CURR:\n##$currdir##"
    #fi
    #exit 0
    if [ "$lastdir" == "EMPTY" ] || [ "$lastdir" == "$currdir" ]; then
        currnum=$lastnum
        currside=$lastside
        currseq=$((lastseq+1))
        newdir=0
    # new floppy
    else
        # determine next image name
        while [ $skipside -gt 0 ]; do
            if [ "$lastside" == "a" ]; then
                currnum=$lastnum
                currside="b"
                currseq=1
            else
                currnum=$((lastnum+1))
                currside="a"
                currseq=1
            fi
            skipside=$((skipside-1))
            lastnum=$currnum
            lastside=$currside
            lastseq=$currseq
        done
        newdir=1
    fi
    # image number with leading zeroes
    currnum=$(printf "%0${dnumbers}d" "$currnum")
    # debug
    #echo "currnum=$currnum# currside=$currside# currseq=$currseq"
    #exit 0
    # image name
    currd64="${dprefix}${currnum}${currside}-${currseq}"
    # create dir file if it doesn't exist
    if [ ! -f "${dprefix}${currnum}${currside}.dir" ]; then
        echo -n "$currdir" > "${dprefix}${currnum}${currside}.dir"
    fi
    # check if the name already exists (it should't happen!)
    if [ -f "${currd64}.d64" ]; then
        echo "Something went wrong, file exists!"
        exit 1
    fi
    # start copying
    echo -e "\n\033[1;34mCreating $currd64\033[0m ...\n"
    #exit 0
    head -n 4 "${dprefix}${currnum}${currside}.dir"
    echo -e "\n"
    d64copy $d64options 8 "${currd64}.d64"
    # check if image exists
    imageok=0
    if [ -f "${currd64}.d64" ]; then
        # check image size
        size=$(wc -c < "${currd64}.d64")
        # get disk name and number of sides
        d64name=$(./pyd64fix.py -i name "${currd64}.d64")
        d64sides=$(./pyd64fix.py -i sides "${currd64}.d64")
        d64moret=$(./pyd64fix.py -i moretracks "${currd64}.d64")
        # if image has no errors
        if [ $size -eq 174848 ] || [ $size -eq 196608 ] || [ $size -eq 205312 ]; then
            # remove -N suffix from the image
            if [ ! -f "${dprefix}${currnum}${currside}.d64" ]; then
                mv "${currd64}.d64" "${dprefix}${currnum}${currside}.d64"
                echo -e "D64 image \033[1;32m${dprefix}${currnum}${currside} OK\033[0m (size: $size, name: $d64name)"
            else
                echo -e "D64 image \033[1;32m$currd64 OK\033[0m (size: $size, name: $d64name)"
            fi
            if [ $dobeep -eq 1 ]; then beep -f 1000 -l 300 -r 2; fi
            imageok=1
        # if image contains errors
        else
            # get number of errors
            errnumber=$(./pyd64fix.py -i errors "${currd64}.d64")
            echo -e "D64 image \033[1;31m$currd64 with $errnumber ERROR(S)\033[0m (size: $size, name: $d64name)"
            if [ $dobeep -eq 1 ]; then beep -f 1000 -l 100 -r 5; fi
            imageok=0
        fi
        # check if the image is maybe one side of double-sided disk
        if [ "$d64sides" == "2" ]; then
            echo -e "\033[1;31mWARNING/ERROR: Maybe double-sided disk?\033[0m"
            imageok=0
        fi
        if [ "$d64moret" == "maybe" ] && [ $tracksno -le 35 ]; then
            echo -e "\033[1;31mWARNING/ERROR: Maybe more than 35 tracks?\033[0m"
            imageok=0
        fi
    fi
    # if not auto-mode, finish the script
    if [ $autod64 -eq 0 ]; then break; fi
    # if image has no errors, wait for next floppy
    if [ $imageok -eq 1 ]; then
        echo -e "\n\033[1;34mInsert next floppy (Ctrl+c for exit)...\033[0m\n"
        # trap keyboard interrupt
        trap control_c SIGINT
        cbmctrl change 8
        trap SIGINT
        if [ $ctrlc -ne 0 ]; then
            echo -e "\033[1;31m\rProgram interrupted, sending reset to IEC...\033[0m"
            cbmctrl reset
            echo
            exit 1
        fi
        sleep 1
    # if image has errors, ask for another try
    else
        echo -n -e "Try copying again [y/n/e-exit]? "
        read -u 10 -n 1 ans
        echo
        if [ "$ans" == "n" ]; then
            echo -e "\n\033[1;34mInsert next floppy (Ctrl+c for exit)...\033[0m\n"
            # trap keyboard interrupt
            trap control_c SIGINT
            cbmctrl change 8
            trap SIGINT
            if [ $ctrlc -ne 0 ]; then
                echo -e "\033[1;31m\rProgram interrupted, sending reset to IEC...\033[0m"
                cbmctrl reset
                echo
                exit 1
            fi
            sleep 1
        elif [ "$ans" == "e" ]; then
            echo -e "\n\033[1;34mExiting...\033[0m\n"
            exit 0
        fi
    fi
    skipside=1
done

# 1.2
#   - Added Skip option
#   - Added getopt exit code check
#   - Added dir files merge option
#   - Added start/end track option
#   - Drive is reset/initialized before other operations
#   - Added examples
#   - Added more checks
# 1.1
#   - Fixed a bug with -m option, .dir as now added to filename when needed
#   - Added more checks for initial image filename
#   - Short skip option changed from -s to -k
#   - Added options for start and end track
#   - Added usage examples
#   - Added more user control to main loop
# 1.0
#   - Initial version

