# PyD64Fix #

Tool for D64 files repair.

![PyD64Fix GUI](./PyD64Fix.png)

## pyd64fix.py ##

This is Python program that tries to combine several d64 images of the same physical floppy, into one image with less or without errors.
Error detection is based on errormap part of d64.

### Usage ###

`pyd64fix.py [-h] [-g] [-a] [-o OUT] [-i INFO] [-u] [d64 [d64 ...]]`

positional arguments:

* `  d64                   (path to) d64 image (with or without d64 extension)`

optional arguments:

* `  -h, --help            show this help message and exit`
* `  -g, --gui             invoke GUI (you need PyQt for this)`
* `  -a, --autoname        auto-generate disk name from its header`
* `  -o OUT, --output OUT  filename for output (.d64 may be omitted)`
* `  -i INFO, --info INFO  print info about d64 file`
* `                        (name,errors,sides,moretracks,desc)`
* `  -u, --usage           Show examples of use`

You may use wildcards for d64 filename(s). Default output filename will have "pyd64fix" suffix.
If there are still errors in d64 file, their number will also be in the filename.

Program will check for different correct blocks in the images.
If they are detected, it is advisable to omit those images from combining, or to do combining manually.
If present, different correct block will have error 16 in errormap of d64 image

Info option will print some information about d64 file, namely:

* `name`       - name and ID of disk
* `errors`     - number of errors in the image
* `sides`      - number of sides of the image (useful to see if double-sided floppy was transferred as single-sided)
* `moretracks` - check if the image maybe has more tracks than 35 (based on BAM in bytes AC and C0 on 18:00)
* `desc`       - number of tracks and errormap bytes in the d64 file

### GUI mode ###

In GUI mode, on the left side you can:

* select which of loaded D64 images should be included in repairing
* select image to be displayed in the main area (one of the loaded, or the composite one)

Main area shows disk blocks of selected image.
For composite image, number of squares inside block correspond to index of source D64 image for that block.
In this area you can:

* hover mouse over a block to show its hex content on the right side of window
* right-click on a block to edit its content on the right side of the window
    * right-click again anywhere on block map to finish editing
    * nine squares indicate user-edited block
* left-click to select next D64 image as block source
    * darker background indicates user-defined selection
    * gray background indicates that selected D64 image does not have that block
    * WARNING: left-click will overwrite user-edited block, no undo!

### Dependencies ###

Program can be used with Python 2 (probably, not tested anymore) or Python 3.
For command-line interface no special libraries are needed (it uses only standard sys, os and argparse libraries), but for GUI you'll need **PySide2** installed.

### Usage examples ###

Windows users probably need to start it with `python pyd64fix.py`.

* `./pyd64fix.py disk001-*.d64`
    * load all d64 files whose name starts with disk001- and combine them into disk001-pyd64fix.d64
    * If, for example, the resulting image still has 3 errors, output name would be disk001-pyd64fix(3E).d64
* `./pyd64fix.py -o newimage a b c`
    * load a.d64, b.d64 and c.d64 and combine them into newimage.d64
* `./pyd64fix.py -g a b.d64 c`
    * load a.d64, b.d64 and c.d64 and start in GUI mode
* `./pyd64fix.py -i sides x.d64`
    * check if x.d64 is maybe made from a double-sided flopy (based on 3rd byte in 18:00 block)
* `./pyd64fix.py -i moretracks x.d64`
    * check if x.d64 is maybe made from a flopy with more than 35 tracks (based on BAM in bytes AC and C0 on 18:00)

### History of changes ###

* 1.4     08.01.2024
    * Added user-selectable d64 image for every block
    * Added hex viewer/editor for individual blocks (based on https://stackoverflow.com/questions/40386194/create-text-area-textedit-with-line-number-in-pyqt)
    * Changed status bar/menu title
    * Added basic GUI help to About dialog
* 1.3     03.03.2023
    * Qt bindings changed from PyQt to PySide2
* 1.2     25.11.2021
    * fixed unused block is counted as error
    * fixed reported number of bad blocks with "-i errors"
    * added track/sector tracking in GUI
    * some python3 and Qt fixes
* 1.1     21.08.2015
    * added check for double-sided image (byte 3 on 18:00)
    * changed prgcall text for windows
    * added info option that prints various informations about d64 file
    * fixed error with whitespace stripping
    * fixed error with indexing of different correct blocks
    * added information about error code for different blocks
    * added rechecking of different blocks in GUI after image (de)selection
    * wider set of chars for normname stripping
    * added check for more than 35 tracks (bytes AC and C0 on 18:00)
    * added tooltips for image names with basic image information
    * image name is colored red if there is maybe more than 35 tracks or if the image is maybe double-sided
    * status bar shows if there is maybe more than 35 tracks or if the image is maybe double-sided
    * when combining images, name and normname are taken from first image with correct 18:0 track
* 1.0 - Initial version

## nextd64.sh ##

Bash script for automating d64 image creation. Script seraches current directory for d64 images matching the pattern dNNNS-Q.d64 where NNN is image number, S is side (a or b), and Q is sequence number of the same image (the format can be changed with -p and -n options).

If the floppy's directory listing is the same as last image, it just incerases sequence number, otherwise it generates next image.

If the image is transfered without errors, sequence number is omitted.

Generated image is checked for presence of markers for more than 35 tracks and for double-sided image (based on BAM in bytes AC and C0 on 18:00).

If image name is given, it will generate next image name starting from there.

### Usage ###

nextd64.sh [options] [image name]

Options:

* `  -k, --skip       - skip disk side (if floppy is single-sided)`
* `  -b, --beep       - beep after copying (requires beep command installed)`
* `  -o, --original   - use original transfer mode instead of warp (d64copy option)`
* `  -r N, --retry N  - number of retries (5 by default)`
* `  -t N, --tracks N - number of tracks, 35, 40 or 42 (35 by default)`
* `  -p P, --prefix P - set another disk image prefix (d by default)`
* `  -n N, --num N    - set another number od image digits (3 by default)`
* `  -s N, --start N  - set start track for copying (1 by default), overrides -t`
* `  -e N, --end N    - set end track for copying (35 by default), overrides -t`
* `  -a, --auto       - auto-wait for next floppy (not available with -s,-e,-t)`
* `  -m, --merge DIR  - merge all dir files into one DIR file`
* `  -x, --examples   - usage examples`
* `  -h, --help       - this help`

### Dependencies ###

Script requires OpenCBM installed on the machine, beep command (only if -b option is used) and PyD64Fix program.

### History of changes ###

* 1.2
    * Added Skip option
    * Added getopt exit code check
    * Added dir files merge option
    * Added start/end track option
    * Drive is reset/initialized before other operations
    * Added examples
    * Added more checks
* 1.1
    * Fixed a bug with -m option, .dir as now added to filename when needed
    * Added more checks for initial image filename
    * Short skip option changed from -s to -k
    * Added options for start and end track
    * Added usage examples
    * Added more user control to main loop
* 1.0
    * Initial version

## Licence ##

Both scripts are released under GPL 3.0.

