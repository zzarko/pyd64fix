#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    PyD64Fix (c) 2015-2024  Žarko Živanov

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

VERSION="1.4"

import sys
import os
import argparse

PETASCII = (" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " ","!",'"',"#","$","%","&","'","(",")","*","+",",","-",".","/",
            "0","1","2","3","4","5","6","7","8","9",":",";","<","=",">","?",
            "@","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O",
            "P","Q","R","S","T","U","V","W","X","Y","Z","[","£","]","↑","←",
            "━","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O",
            "P","Q","R","S","T","U","V","W","X","Y","Z","┼"," ","│"," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
            " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ")

# converts PETASCII to ASCII
def toascii(s):
    return "".join(PETASCII[c] for c in s)

# converts bytes/string to hex numbers
def hexdata(s):
    return " ".join("{:02x}".format(c) for c in s)

# contains data for one 256-byte block
class D64Block(object):
    def __init__(self, data=None):
        self.data = data

    def __setattr__(self, name, value):
        # override assignment to make a copy
        if (name == "data") and (value != None):
            self.__dict__[name] = bytearray(value)
        else:
            super(D64Block, self).__setattr__(name, value)

    def __eq__(self, other):
        return self.data == other.data

    def __ne__(self, other):
        return self.data != other.data

    def load(self, descriptor):
        self.data = bytearray(descriptor.read(256))

    def zeroes():
        self.data = bytearray(256)

    def __getitem__(self, items):
        return self.data[items]

    def __str__(self):
        return hexdata(self.data)

    def __len__(self):
        return len(self.data)

# contains the data for one d64 image
class D64Data(object):
    # number of sectors in each track (1-42)
    sectors = (21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,
               19,19,19,19,19,19,19,
               18,18,18,18,18,18,
               17,17,17,17,17,17,17,17,17,17,17,17)

    # starting block for tracks
    sectorsn= (0,21, 42, 63, 84, 105, 126, 147, 168, 189, 210, 231,
               252, 273, 294, 315, 336, 357, 376, 395, 414, 433, 452,
               471, 490, 508, 526, 544, 562, 580, 598, 615, 632, 649,
               666, 683, 700, 717, 734, 751, 768, 785)

    # valid sizes of d64 images
    d64sizes = {174848:(35, 683, False, "35 tracks, no errors"),
                175531:(35, 683, True, "35 track, 683 error bytes"),
                196608:(40, 768, False, "40 track, no errors"),
                197376:(40, 768, True, "40 track, 768 error bytes"),
                205312:(42, 802, False, "42 track, no errors"),
                206114:(42, 802, True, "42 track, 802 error bytes")}

    def __init__(self, filepath = ""):
        self.data = []                  # d64 bytes
        self.tracks = 0                 # 35, 40 or 42
        self.blocks = 0                 # 683, 768 or 802
        self.errors = False             # does the image contains errormap
        self.errorcount = 0             # number of errors
        self.errormap = bytearray(0)    # errormap
        self.description = ""           # image description
        self.name = ""                  # name extracted from image
        self.normname = ""              # normalised name
        self.filename = "PyD64Fix.d64"  # name of the d64 file
        self.use = True                 # if the image can be used in combining
        self.used = False               # if the image is used in combining
        self.single = True              # if the image maybe is from single-sided disk
        self.moretracks = False         # if the image maybe has more than 35 tracks
        self.selectmap = None
        self.custommap = [None]*803
        if filepath != "": self.load(filepath)

    # load d64 file specified by filepath (.d64 may be omitted)
    def load(self, filepath):
        if not os.path.exists(filepath):
            if filepath[-4:0] in [".d64",".D64"]:
                return False
            else:
                filepath += ".d64"
                if not os.path.exists(filepath):
                    return False
        # check file size
        d64fstat = os.stat(filepath)
        if not d64fstat.st_size in self.d64sizes:
            return False
        # setup basic d64 informations
        self.tracks = self.d64sizes[d64fstat.st_size][0]
        self.blocks = self.d64sizes[d64fstat.st_size][1]
        self.errors = self.d64sizes[d64fstat.st_size][2]
        self.description = self.d64sizes[d64fstat.st_size][3]
        try:
            d64file = open(filepath, mode="rb")
            # load d64 blocks
            for b in range(self.blocks):
                block = D64Block()
                block.load(d64file)
                self.data.append(block)
            # load error information, if exists
            if self.errors:
                self.errormap = bytearray(1)
                self.errormap.extend( bytearray(d64file.read(self.blocks)) )
                self.errorcount = self.blocks + 1 - list(self.errormap).count(1) - list(self.errormap).count(0) # +1 - errormap has 1 byte extra
            else:
                self.errorcount = 0
            # filename
            self.filenamed64 = os.path.basename(filepath)
            # filename without extension
            self.filename = os.path.splitext(self.filenamed64)[0]
            # extract disk name and ID from d64
            self.name = toascii(self[18,0][144:164])
            # check if maybe double-sided disk
            self.single = self[18,0][3] != 0x80
            # check if maybe 40 or 42 track disk (Dolphin/Speed Dos)
            self.moretracks = (self[18,0][0xac] != 0) or (self[18,0][0xc0] != 0)
            # normalized disk name
            self.normname = self.name
            self.normname = self.normname.strip(" -_<>?:;+\"'*/")
            self.normname = self.normname.replace(" ","_")
            while self.normname.find("__") != -1: self.normname = self.normname.replace("__","_")
        except:
            self.data = []
            return False
        finally:
            d64file.close()

        return True

    # allow indexing/slicing of blocks
    def __getitem__(self, items):
        if isinstance(items, tuple):
            # track 1-42, sector 0-20
            assert (len(items) == 2) and (1 <= items[0] <= self.tracks) and (0 <= items[1] < self.sectors[items[0]]), "track/sector out of range (%d, %d)" % (items[0],items[1])
            return self.data[ self.sectorsn[items[0]-1]+items[1] ]
        elif isinstance(items, slice):
            # block 1-682/768
            return self.data[slice(items.start-1, items.stop-1,items.step)]
        else:
            # block 1-682/768
            assert 1 <= items <= self.blocks, "block out of range (%d)" % items
            return self.data[items-1]

# collection of d64 images from same physical floppy
class D64Collection(object):
    def __init__(self):
        self.clear()
        self.combined = D64Data()
    def clear(self):
        self.data = []
        self.different = [] # indexes of correct blocks that are different
        self.combname = ""
        self.combined = None

    # add a d64 image, check if correct blocks are the same
    def add(self, d64):
        if isinstance(d64, str):
            d64 = D64Data(d64)
            if len(d64.data) == 0: return None
        if len(self.data) > 0:
            if d64.tracks != self.data[0].tracks: return None
        self.data.append(d64)
        if self.combname == "": self.combname = d64.filename
        else:
            i = 0
            while (len(self.combname) > i) and (len(d64.filename) > i) and (self.combname[i] == d64.filename[i]): i += 1
            self.combname = self.combname[:i]
        if self.combname != "":
            while self.combname[-1] in "- , . :".split(): self.combname = self.combname[:-1]
        # check if correct blocks are the same
        different = []
        for d in self.data[:-1]:    # iterate through d64 images
            for i,b in enumerate(d[1:d.blocks+1]):  # iterate through blocks
                check = True
                if d.errors and d.errormap[i+1] != 1: check = False
                if d64.errors and d64.errormap[i+1] != 1: check = False
                if check and (b != d64[i+1]):
                    different.append(i+1)
                    d64.errormap[i+1] = 16
        self.different.append(different)
        self.combine()
        return different

    def recheck_different(self):
        # clear current differences
        del self.different[:]
        for j,d64 in enumerate(self.data):
            # clear different flags
            different = []
            for i,e in enumerate(d64.errormap):
                if e == 16: d64.errormap[i] = 1
            if d64.use:
                for d in self.data[:j]:
                    if d.use:
                        for i,b in enumerate(d[1:d.blocks+1]):  # iterate through blocks
                            check = True
                            if d.errors and d.errormap[i+1] != 1: check = False
                            if d64.errors and d64.errormap[i+1] != 1: check = False
                            if check and (b != d64[i+1]):
                                different.append(i+1)
                                d64.errormap[i+1] = 16
            self.different.append(different)

    # try to combine loaded images into one correct image
    def combine(self, filepath = "", auto=True):
        if self.data == []: return False
        self.combined = D64Data()
        emptylist = not auto
        for d in self.data:
            d.used = False
            if auto: d.use = True
            elif d.use: emptylist = False
        if emptylist:
            self.combined.errormap = bytearray(1+self.data[0].blocks)
            return False
        self.combined.blocks = self.data[0].blocks
        self.combined.tracks = self.data[0].tracks
        # find first image with correct 18:0 block
        for d in self.data:
            if d.use and ((not d.errors) or (d.errormap[357] == 1)): break
        else:
            # or first image in use
            for d in self.data:
                if d.use: break
        self.combined.name = d.name
        self.combined.normname = d.normname
        self.combined.errormap = bytearray(1)
        self.combined.errorcount = 0
        self.combined.selectmap = [0]
        self.combined.custommap = [None]*803

        for i in range(1,self.data[0].blocks+1):
            difcheck = True
            added = False
            # check if there is different correct blocks for index
            for j,d in enumerate(self.data):
                if i in self.different[j] and d.use:
                    difcheck = False
                    break
            # copy one correct block, if exists
            for dnum, d in enumerate(self.data):
                if ((not d.errors) or (d.errormap[i] == 1)) and d.use:
                    self.combined.data.append(d[i])
                    self.combined.selectmap.append(dnum)
                    d.used = True
                    added = True
                    break
            else:
                #TODO choose least bad error, not first from the list
                # if there aren't correct blocks for index, copy first incorrect block
                # find first used image with copied block
                for dnum, d in enumerate(self.data):
                    if d.use and d.errormap[i] != 0: break
                else:
                    # or find first used image
                    for dnum, d in enumerate(self.data):
                        if d.use and d.errormap[i] != 0: break
            # if everything is OK
            if difcheck and added:
                self.combined.errormap.append(1)
            else:
                # there are still errors...
                self.combined.errors = True
                self.combined.errorcount += 1
                if not added:
                    self.combined.data.append(d[i])
                    self.combined.selectmap.append(dnum)
                # 16 indicates different correct blocks
                if not difcheck: self.combined.errormap.append(16)
                else: self.combined.errormap.append(d.errormap[i])
        # save combined d64 image
        if filepath != "": self.save(filepath)
        return not self.combined.errors

    def save(self, filepath):
        if self.combined:
            ok = True
            try:
                d64file = open(filepath, mode="wb")
                for b in self.combined.data:
                    d64file.write(b.data)
                if self.combined.errors:
                    d64file.write(self.combined.errormap[1:])
            except:
                ok = False
            finally:
                d64file.close()
        return ok

about = u"""PyD64Fix %s (c) 2024  Zarko Zivanov

Program that tries to combine several d64 images
of the same physical floppy, into one image with
less or without errors.
Error detection is based on errormap part of d64.""" % (VERSION)

epilog="""You may use wildcards for d64 filename(s).
Default output filename will have "pyd64fix" suffix.
If there are still errors in d64 file, their number
will also be in the filename.

Program will check for different correct blocks in
the images. If they are detected, it is advisable to
omit those images from combining, or to do combining
manually. If present, different correct block will
have error 16 in errormap of d64 image"""

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=about, formatter_class=argparse.RawDescriptionHelpFormatter,epilog=epilog)
    parser.add_argument('d64', metavar='d64', nargs='*', default="", help='(path to) d64 image (with or without d64 extension)')
    parser.add_argument('-g','--gui', action='store_true', help='invoke GUI (you need PyQt for this)')
    parser.add_argument('-a','--autoname', action='store_true', help='auto-generate disk name from its header')
    parser.add_argument('-o','--output', metavar="OUT", help='filename for output (.d64 may be omitted)')
    parser.add_argument('-i','--info', metavar="INFO", choices=['name','errors','sides','moretracks','desc'], help='print info about d64 file (name,errors,sides,moretracks,desc)')
    parser.add_argument('-u','--usage', action='store_true', help='Show examples of use')
    args = parser.parse_args()

    prgcall = "python pyd64fix.py" if sys.platform[:3] == "win" else "./pyd64fix.py"
    if args.usage:
        print("""Usage examples:

%(p)s disk001-*.d64
    load all d64 files whose name starts with disk001-
    and combine them into disk001-pyd64fix.d64. If, for
    example, the resulting image still has 3 errors,
    output name would be disk001-pyd64fix(3E).d64

%(p)s -o newimage a b c
    load a.d64, b.d64 and c.d64 and combine them into
    newimage.d64

%(p)s -g a b.d64 c
    load a.d64, b.d64 and c.d64 and start in GUI mode

%(p)s -i sides x.d64
    check if x.d64 is maybe made from a double-sided flopy
    (based on 3rd byte in 18:00 block)

%(p)s -i moretracks x.d64
    check if x.d64 is maybe made from a flopy with more than 35 tracks
    (based on BAM in bytes AC and C0 on 18:00)
    """ % {'p' : prgcall})
        exit(0)

    d64col = D64Collection()
    for d in args.d64:
        if not args.info: print("Loading %s ..." % d, end="")
        sys.stdout.flush()
        dif = d64col.add(d)
        if dif != None:
            if not args.info: print("\rLoaded %s" % d64col.data[-1].filenamed64, end="")
        if dif == None:
            if not args.info: print(", Not found or wrong size")
        elif dif != []:
            if not args.info: print(", Different blocks detected", end="")
        elif not d64col.data[-1].single:
            if not args.info: print(", Possibly double-sided disk", end="")
        elif d64col.data[-1].moretracks:
            if not args.info: print(", Possibly more than 35 tracks", end="")
        else:
            if not args.info: print(", OK", end="")
        if dif != None:
            if d64col.data[-1].errors:
                if not args.info: print(", %d error(s)" % d64col.data[-1].errorcount)
            else:
                if not args.info: print(", no errors")
    if args.info and len(d64col.data) > 0:
        if args.info == "name":
            print(d64col.data[-1].normname)
        elif args.info == "errors":
            print(d64col.data[-1].errorcount)
        elif args.info == "desc":
            print(d64col.data[-1].description)
        elif args.info == "sides":
            print(1 if d64col.data[-1].single else 2)
        elif args.info == "moretracks":
            print("maybe" if d64col.data[-1].moretracks else "no")
        exit(0)
    
    #TODO for autoname find first d64 with correct 18:0 block, not first from the list
    if args.output != None:
        output = args.output
    elif args.autoname: output = d64col.data[0].normname
    elif d64col.combname != "": output = d64col.combname+"-pyd64fix"
    else: output = "pyd64fix"
    if not args.gui:
        if d64col.data != []:
            if d64col.combined.errors: output += "(%dE)" % d64col.combined.errorcount
            if not output[-4:0] in [".d64",".D64"]: output += ".d64"
            d64col.save(output)
            print("Saved as", output, ", with %d error(s)" % d64col.combined.errorcount if d64col.combined.errors else ", no errors")
            exit(1 if d64col.combined.errors else 0)
        else:
            parser.parse_args(["-h"])
            exit(0)

from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2.QtWidgets import *

dice = (
    ((1,1),), # 1
    ((0,0),(2,2)), # 2
    ((0,0),(1,1),(2,2)), # 3
    ((0,0),(0,2),(2,0),(2,2)), # 4
    ((0,0),(0,2),(1,1),(2,0),(2,2)), # 5
    ((0,0),(0,2),(0,1),(2,1),(2,0),(2,2)), # 6
    ((0,0),(0,2),(0,1),(1,1),(2,1),(2,0),(2,2)), # 7
    ((0,0),(0,1),(0,2),(1,0),(2,1),(2,0),(1,2),(2,2)), # 8
    ((0,0),(0,1),(0,2),(1,0),(1,1),(2,1),(2,0),(1,2),(2,2)), # 9
)


##############################################################################
# QCodeEditor with line numbers by acbetter adapted from:
# https://stackoverflow.com/questions/40386194/create-text-area-textedit-with-line-number-in-pyqt

class QLineNumberArea(QWidget):
    def __init__(self, editor):
        super().__init__(editor)
        self.codeEditor = editor

    def sizeHint(self):
        return QSize(self.codeEditor.lineNumberAreaWidth(), 0)

    def paintEvent(self, event):
        self.codeEditor.lineNumberAreaPaintEvent(event)


class QCodeEditor(QPlainTextEdit):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.lineNumberArea = QLineNumberArea(self)
        self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
        self.updateRequest.connect(self.updateLineNumberArea)
        self.cursorPositionChanged.connect(self.highlightCurrentLine)
        self.textChanged.connect(self.checkText)
        self.updateLineNumberAreaWidth(0)
        self.lineSelection = QTextEdit.ExtraSelection()
        lineColor = QColor(Qt.yellow).lighter(160)
        self.lineSelection.format.setBackground(lineColor)
        self.lineSelection.format.setProperty(QTextFormat.FullWidthSelection, True)
        self.badSelection = None

    def checkText(self):
        if not self.isReadOnly():
            try:
                data = bytearray.fromhex(self.toPlainText())
                #print("CHANGED")
                self.badSelection = None
            except ValueError as e:
                epos = int(str(e).split()[-1])
                #print("BAAAAD", epos)
                self.badSelection = QTextEdit.ExtraSelection()
                lineColor = QColor(Qt.red)
                self.badSelection.format.setBackground(lineColor)
                self.badSelection.format.setProperty(QTextFormat.FullWidthSelection, True)
                self.badSelection.cursor = self.textCursor()
                self.badSelection.cursor.setPosition(epos)
                self.badSelection.cursor.movePosition(QTextCursor.NextCharacter, QTextCursor.KeepAnchor)

            extraSelections = []
            extraSelections.append(self.lineSelection)
            if self.badSelection:
                extraSelections.append(self.badSelection)
            self.setExtraSelections(extraSelections)

    def lineNumberAreaWidth(self):
        digits = 2
        space = 3 + self.fontMetrics().horizontalAdvance('9') * digits
        return space

    def sizeHint(self):
        return QSize(self.lineNumberAreaWidth() + self.fontMetrics().horizontalAdvance('9')*26, 0)

    def updateLineNumberAreaWidth(self, _):
        self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)

    def updateLineNumberArea(self, rect, dy):
        if dy:
            self.lineNumberArea.scroll(0, dy)
        else:
            self.lineNumberArea.update(0, rect.y(), self.lineNumberArea.width(), rect.height())
        if rect.contains(self.viewport().rect()):
            self.updateLineNumberAreaWidth(0)

    def resizeEvent(self, event):
        super().resizeEvent(event)
        cr = self.contentsRect()
        self.lineNumberArea.setGeometry(QRect(cr.left(), cr.top(), self.lineNumberAreaWidth(), cr.height()))

    def highlightCurrentLine(self):
        extraSelections = []
        if not self.isReadOnly():
            self.lineSelection.cursor = self.textCursor()
            self.lineSelection.cursor.clearSelection()
            extraSelections.append(self.lineSelection)
        if self.badSelection:
            extraSelections.append(self.badSelection)
        self.setExtraSelections(extraSelections)

    def lineNumberAreaPaintEvent(self, event):
        painter = QPainter(self.lineNumberArea)

        painter.fillRect(event.rect(), Qt.lightGray)

        block = self.firstVisibleBlock()
        blockNumber = block.blockNumber()
        top = self.blockBoundingGeometry(block).translated(self.contentOffset()).top()
        bottom = top + self.blockBoundingRect(block).height()

        # Just to make sure I use the right font
        height = self.fontMetrics().height()
        while block.isValid() and (top <= event.rect().bottom()):
            if block.isVisible() and (bottom >= event.rect().top()):
                number = f"{(blockNumber*8):02X}" #str(blockNumber + 1)
                painter.setPen(Qt.black)
                painter.drawText(0, top, self.lineNumberArea.width(), height, Qt.AlignRight, number)

            block = block.next()
            top = bottom
            bottom = top + self.blockBoundingRect(block).height()
            blockNumber += 1
##############################################################################

class d64DataWidget(QWidget):

    signalTrackSector = Signal()
    signalActivateEditor = Signal()
    signalChangeBlock = Signal()

    def __init__(self):
        super(d64DataWidget, self).__init__()
        self.initUI()

    def initUI(self):
        self.setMinimumSize(100, 100)
        self.tracks = 42
        self.errormap = [0]*803 #802 + 1, indexing starts from 1
        self.errorcol = [Qt.red] * 256
        self.errorcol[0] = Qt.NoBrush
        self.errorcol[1] = Qt.green
        self.errorcol[16] = Qt.blue
        
        self.track = 0
        self.sector = 0
        self.trackX = None
        self.sectorX = None
        self.blockChanged = False
        self.mapPos = None
        self.blockW = 0
        self.blockH = 0

        self.errormap = None
        self.selectmap = None
        self.custommap = None

        self.installEventFilter(self)
        self.setMouseTracking(True)

    def mousePressEvent(self, event):
        self.mapPos = None
        if self.selectmap and (self.track > 0) and (self.sector > 0):
            # find block position
            pos = self.sector + self.d64.sectorsn[self.track-1]
            if pos < len(self.selectmap):
                self.mapPos = pos
                if event.button() == Qt.LeftButton:
                    #print("L CLICK", self.track, self.sector)
                    self.signalChangeBlock.emit()
                elif event.button() == Qt.RightButton:
                    #print("R CLICK", self.track, self.sector)
                    self.signalActivateEditor.emit()

    def setD64(self, d64):
        self.errormap = [0]*803
        self.d64 = d64
        if d64.errors:
            self.errormap[0:len(d64.errormap)] = d64.errormap
        else:
            self.errormap[1:d64.blocks+1] = [1]*(d64.blocks+1)
        self.selectmap = d64.selectmap
        self.custommap = d64.custommap
        self.trackX = None
        self.sectorX = None
        self.update()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()

    def eventFilter(self, widget, event):
        if event.type() == QEvent.MouseMove:
            if (self.trackX != self.track) or (self.sectorX != self.sector):
                self.blockChanged = True
            self.trackX = self.track
            self.sectorX = self.sector
            self.track = 0
            self.sector = 0
            pos = event.pos()
            if self.blockW != 0:
                self.track = pos.x() // self.blockW
                if self.track < 42:
                    if self.blockH != 0:
                        self.sector = pos.y() // self.blockH
                        if self.sector >= D64Data.sectors[self.track]:
                            self.track = 0
                            self.sector = 0
                        else:
                            self.track += 1
                            self.sector += 1
                else:
                    self.track = 0
                    self.sector = 0
            #print(self.track,self.sector)
            self.signalTrackSector.emit()
        return QWidget.eventFilter(self, widget, event)

    def drawWidget(self, qp):
        size = self.size()
        w = size.width()
        h = size.height()

        stepx = w // self.tracks
        stepy = h // 21
        self.blockW = stepx
        self.blockH = stepy

        qp.setPen(QColor(0, 0, 0))

        xx = 0
        erridx = 1
        for x in range(self.tracks):
            yy = 0
            for y in range(D64Data.sectors[x]):
                if (self.custommap[erridx] == None) or (self.errormap[erridx] == 0):
                    qp.setBrush(self.errorcol[self.errormap[erridx]])
                else:
                    qp.setBrush(QColor(self.errorcol[self.errormap[erridx]]).darker(135))
                qp.drawRect(xx, yy, stepx-2, stepy-2)
                yy = yy + stepy
                erridx += 1
            xx = xx + stepx

        if self.selectmap:
            xx = 0
            mapidx = 1
            qp.setBrush(QColor(0, 0, 0))
            sqx = stepx // 3
            sqy = stepy // 3
            numx = [2, 2 + sqx, 2 + sqx + sqx]
            numy = [2, 2 + sqy, 2 + sqy + sqy]
            sqx = max(2, sqx-4)
            sqy = max(2, sqy-4)
            #print(f"{len(self.selectmap)} {self.tracks}", end='')
            for x in range(self.tracks):
                yy = 0
                #print(f"\n{D64Data.sectors[x]} {mapidx}->{D64Data.sectors[x]+mapidx} ",end='')
                for y in range(D64Data.sectors[x]):
                    #print(f"{self.selectmap[mapidx]} ",end='')
                    d64Idx = self.custommap[mapidx] if self.custommap[mapidx] != None else self.selectmap[mapidx]
                    for dc in dice[d64Idx]:
                        qp.drawRect(xx+numx[dc[0]], yy+numy[dc[1]], sqx, sqy)
                    yy = yy + stepy
                    mapidx += 1
                xx = xx + stepx
                if mapidx >= len(self.selectmap): break

class D64List(QGridLayout):
    def __init__(self, parent=None):
        super(D64List, self).__init__(parent)
        self.parent1 = parent
        label = QLabel("Sh")
        label.setToolTip("Show image")
        self.addWidget(label, 0, 0, alignment=Qt.AlignCenter)
        label = QLabel("Cm")
        label.setToolTip("Use image for combining")
        self.addWidget(label, 0, 1, alignment=Qt.AlignCenter)
        label = QLabel("d64 image")
        label.setToolTip("Image's filename")
        self.addWidget(label, 0, 2)
        self.rlist = []
        self.clist = []
        self.llist = []
        self.row = 1
        self.sgroup = QButtonGroup()
        self.sgroup.setExclusive(True)
        self.cgroup = QButtonGroup()
        self.cgroup.setExclusive(False)
        self.setContentsMargins(0,0,0,0)

    def clear(self):
        for d in self.rlist:
            self.sgroup.removeButton(d)
            d.close()
        for d in self.clist:
            self.cgroup.removeButton(d)
            d.close()
        for d in self.llist:
            self.removeWidget(d)
            d.close()
        self.rlist = []
        self.clist = []
        self.llist = []
        self.row = 1
        self.update()

    def add(self, d64, combineit=True):
        radio = QRadioButton()
        name = d64.filename
        if name[-4:] in [".d64",".D64"]: name = name[:-4]
        if len(name) > 16: name = name[0:16]
        self.sgroup.addButton(radio, self.row-1)
        self.addWidget(radio, self.row, 0, alignment=Qt.AlignCenter)
        radio.setToolTip("Show "+name+" image")
        self.rlist.append(radio)
        check = QCheckBox()
        if not combineit: check.setEnabled(False)
        self.cgroup.addButton(check, self.row-1)
        self.addWidget(check, self.row, 1, alignment=Qt.AlignCenter)
        check.setToolTip("Use "+name+" image for combining")
        self.clist.append(check)
        label = QLabel(f"[{len(self.llist)+1}]{name}")
        self.addWidget(label, self.row, 2)
        self.llist.append(label)
        label.setToolTip('"'+d64.normname+'"' + (", "+d64.description if d64.description != "" else "") + ("" if d64.single else ", maybe double-sided") + (", maybe more than 35 tracks" if d64.moretracks else ""))
        if (not d64.single) or d64.moretracks:
            label.setStyleSheet("QLabel { color : red; }")
        #if self.sgroup.checkedId() == -1: radio.setChecked(True)
        radio.setChecked(True)
        if combineit: check.setChecked(True)
        self.row += 1

class MainWindow(QMainWindow):
    """main application window"""
    def __init__(self, parent=None): 
        super(MainWindow, self).__init__(parent)

        self.setWindowTitle("PyD64Fix")

        self.centralWidget = QWidget()
        self.centralWidgetLayout = QHBoxLayout()
        self.centralWidget.setLayout(self.centralWidgetLayout)
        self.setCentralWidget(self.centralWidget)

        fixedFont = QFont("monospace")
        fixedFont.setStyleHint(QFont.TypeWriter)

        self.statusBar = QStatusBar(self)
        self.statusBar.setObjectName("setStatusbar")
        self.setStatusBar(self.statusBar)
        self.statusD64 = D64Data()
        self.statusBlock = QLabel("Track:00 Sector:00")
        self.statusBlock.setFont(fixedFont)
        self.statusBar.addPermanentWidget(self.statusBlock)

        #widget for controls
        self.controls = QVBoxLayout()
        self.controls.setAlignment(Qt.AlignTop)
        self.controls.setContentsMargins(0,0,0,0)
        self.centralWidgetLayout.addLayout(self.controls)

        button = QPushButton("Select d64")
        self.controls.addWidget(button)
        button.setToolTip("Select (multiple) d64 files of the same physical floppy")
        self.connect(button, SIGNAL("clicked()"), self.selectD64)

        button = QPushButton("Save d64")
        self.controls.addWidget(button)
        button.setToolTip("Save combined d64 image")
        self.connect(button, SIGNAL("clicked()"), self.saveD64)

        button = QPushButton("Help/About")
        self.controls.addWidget(button)
        self.connect(button, SIGNAL("clicked()"), self.about)

        # d64 list
        self.d64list = D64List()
        self.controls.addLayout(self.d64list)
        for d64 in d64col.data:
            self.d64list.add(d64)
        self.d64list.add(d64col.combined, False)
        self.connect(self.d64list.sgroup, SIGNAL("buttonClicked(int)"), self, SLOT("radioClickedSlot(int)"))
        self.connect(self.d64list.cgroup, SIGNAL("buttonClicked(int)"), self, SLOT("checkClickedSlot(int)"))

        #widget for d64 representation
        self.d64Layout = QVBoxLayout()
        self.d64Layout.setContentsMargins(0,0,0,0)
        self.centralWidgetLayout.addLayout(self.d64Layout)
        self.d64widget = d64DataWidget()
        self.d64Layout.addWidget(self.d64widget)
        self.d64widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.d64widget.signalTrackSector.connect(self.showTrackSector)
        self.d64label = QLabel(u"<b><font color='green'>■<font color='white'> - OK"
                               u", <font color='red'>■<font color='white'> - Error"
                               u", <font color='blue'>■<font color='white'> - Different data"
                               u", <font color='black'>□<font color='white'> - Unused")
        self.d64label.setStyleSheet("background-color: darkgray")
        self.d64Layout.addWidget(self.d64label)
        self.resize(QSize(500,200))

        self.d64widget.signalActivateEditor.connect(self.activateEditor)
        self.d64widget.signalChangeBlock.connect(self.changeBlock)

        self.d64list.rlist[-1].click()
        self.selectPath = os.getcwd()
        self.savePath = self.selectPath

        self.editor = QCodeEditor()
        self.editor.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.centralWidgetLayout.addWidget(self.editor)
        self.editor.setFont(fixedFont)
        self.editor.setReadOnly(True)
        self.editor.setOverwriteMode(True)
        self.editorActive = False
        self.editorBlock = None

    def selectD64(self):
        dialog = QFileDialog(None,"Select d64 file(s) for combining",self.selectPath,"d64 images (*.d64 *.D64)")
        dialog.setFileMode(QFileDialog.ExistingFiles)
        dialog.setAcceptMode(QFileDialog.AcceptOpen)
        if dialog.exec_() == QDialog.Accepted:
            #if user selected something
            d64col.clear()
            for d in dialog.selectedFiles():
                #d64col.add(unicode(d))
                d64col.add(d)
            self.selectPath = dialog.directory().path()
            self.d64list.clear()
            for d64 in d64col.data:
                self.d64list.add(d64)
            self.d64list.add(d64col.combined, False)
            self.d64list.rlist[-1].click()

    def saveD64(self):
        dialog = QFileDialog(None,"Save combined d64 file",self.savePath,"d64 images (*.d64 *.D64)")
        dialog.setFileMode(QFileDialog.AnyFile)
        errors = "(%dE)" % d64col.combined.errorcount if d64col.combined.errors else ""
        if d64col.combname == "": dialog.selectFile("pyd64fix%s.d64" % errors)
        else: dialog.selectFile(d64col.combname + "-pyd64fix%s.d64" % errors)
        dialog.setAcceptMode(QFileDialog.AcceptSave)
        if dialog.exec_() == QDialog.Accepted:
            #if user selected something
            d64col.save(dialog.selectedFiles()[0])
            self.savePath = dialog.directory().path()

    @Slot(int)
    def radioClickedSlot(self,index):
        if index < len(d64col.data):
            self.d64widget.setD64(d64col.data[index])
            self.statusD64 = d64col.data[index]
            self.showD64Info()
        elif d64col.combined:
            self.d64widget.setD64(d64col.combined)
            self.statusD64 = d64col.combined
            self.showD64Info()

    @Slot(int)
    def checkClickedSlot(self,index):
        d64col.data[index].use = self.d64list.cgroup.button(index).isChecked()
        d64col.recheck_different()
        d64col.combine(auto=False)
        self.radioClickedSlot(self.d64list.sgroup.checkedId())

    def activateEditor(self):
        if self.editorActive:
            try:
                data = D64Block(bytearray.fromhex(self.editor.toPlainText()))
            except ValueError as e:
                self.statusBar.showMessage("ERROR: Invalid hex number(s) in block")
                return
            if len(data) == 256:
                if d64col.combined.data[self.editorBlock] != data:
                    #print(bytearray.fromhex(self.editor.toPlainText()))
                    #print(self.editorBlock)
                    d64col.combined.data[self.editorBlock] = data
                    d64col.combined.custommap[self.editorBlock+1] = 8
                    d64col.combined.errormap[self.editorBlock+1] = 1
                    self.d64widget.errormap[self.editorBlock+1] = 1
                    self.statusBar.showMessage("Block changed")
                    self.d64widget.update()
                else:
                    self.statusBar.showMessage("Block unchanged")
                self.editor.setReadOnly(True)
                self.editor.highlightCurrentLine()
                self.editorActive = False
                self.editorBlock = None
            else:
                self.statusBar.showMessage("ERROR: Block must have EXACTLY 256 bytes!")
        else:
            if (self.d64widget.mapPos != None) and (self.d64widget.d64 == d64col.combined):
                self.editorActive = True
                self.editor.setReadOnly(False)
                self.editor.highlightCurrentLine()
                self.editorBlock = self.d64widget.mapPos - 1

    def changeBlock(self):
        if self.d64widget.d64 == d64col.combined:
            blkPos = self.d64widget.mapPos
            imgPos = d64col.combined.custommap[blkPos]
            if imgPos == None:
                imgPos = d64col.combined.selectmap[blkPos] + 1
            else:
                imgPos += 1
            if imgPos >= len(d64col.data):
                imgPos = 0
            if imgPos == d64col.combined.selectmap[blkPos]:
                imgPos = None
            finalPos = imgPos if imgPos != None else d64col.combined.selectmap[blkPos]
            d64col.combined.data[blkPos - 1] = d64col.data[finalPos].data[blkPos - 1]
            d64col.combined.errormap[blkPos] = d64col.data[finalPos].errormap[blkPos]
            self.d64widget.errormap[blkPos] = d64col.data[finalPos].errormap[blkPos]
            d64col.combined.custommap[blkPos] = imgPos
            self.d64widget.update()
            self.showTrackSector()

    def showD64Info(self, message=""):
        if self.statusD64.blocks > 0:
            maybe = "%s%s" % (("" if self.statusD64.single else ", double-sided?"), (", >35 tracks?" if self.statusD64.moretracks else ""))
            self.setWindowTitle(f"PyD64Fix: '{self.statusD64.name}', {self.statusD64.blocks} blocks, {self.statusD64.errorcount} errors{maybe}")
        else:
            self.setWindowTitle(f"PyD64Fix")

    def showTrackSector(self):
        if not self.editorActive:
            d64 = self.d64widget.d64
            self.statusBlock.setText(f"Track:{self.d64widget.track:02} Sector:{self.d64widget.sector:02}")
            if self.d64widget.blockChanged:
                self.d64widget.blockChanged = False
            if (self.d64widget.track > 0) and (self.d64widget.sector > 0):
                # find block position
                pos = self.d64widget.sector-1 + d64col.combined.sectorsn[self.d64widget.track-1]
                if pos < len(d64.data):
                    rows = [" ".join([f"{x:02X}" for x in d64.data[pos][i:i+8]]) for i in range(0, 256,8)]
                    self.editor.setPlainText("\n".join(rows))
                else:
                    self.editor.setPlainText("")
            else:
                self.editor.setPlainText("")

    def about(self):
        """displays about dialog"""
        text = "Number of squares inside block correspond to index of source D64 image for that block."
        text += "<br><br>Right-click to edit block content."
        text += " Right-click again anywhere on block map to finish editing."
        text += " Nine squares indicate user-edited block."
        text += "<br><br>Left-click to select next D64 image as block source."
        text += " Darker background indicates user-defined selection."
        text += " Gray background indicates that selected D64 image does not have that block."
        text += "<br>WARNING: Left-click will overwrite user-edited block, no undo!"

        text += "<br>-------------------------------------------------------------------<br>"
        text += about.replace("\n","<br>")

        text += "<br><br>E-Mail: zzarko@gmail.com, zzarko@lugons.org"
        text += "<br><br>Once Upon a Byte"
        text += ', <a href="http://www.onceuponabyte.org/">www.onceuponabyte.org</a>'
        text += "<br><br>Linux User Group of Novi Sad"
        text += ', <a href="http://www.lugons.org/">http://www.lugons.org/</a>'
        text += "<br><br>Many thanks to Marko for testing!"

        gpl = "<br><br>This program is free software: you can redistribute it and/or modify "
        gpl += "it under the terms of the GNU General Public License as published by "
        gpl += "the Free Software Foundation, either version 3 of the License, or "
        gpl += "(at your option) any later version."
        gpl += "<br><br>This program is distributed in the hope that it will be useful, "
        gpl += "but WITHOUT ANY WARRANTY; without even the implied warranty of "
        gpl += "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
        gpl += "GNU General Public License for more details. "
        gpl += "<br><br>You should have received a copy of the GNU General Public License "
        gpl += "along with this program. If not, see "
        gpl += "<a href=http://www.gnu.org/licenses>http://www.gnu.org/licenses</a>."

        dialog = QMessageBox(QMessageBox.Information,"Help/About",text+gpl)
        dialog.setTextFormat(Qt.RichText)
        dialog.exec_()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MainWindow()
    main.show()
    app.exec_()

"""
History of changes

1.4     08.01.2024
 - added user-selectable d64 image for every block
 - added hex viewer/editor for individual blocks (based on https://stackoverflow.com/questions/40386194/create-text-area-textedit-with-line-number-in-pyqt)
 - changed status bar/menu title
 - added basic GUI help to About dialog

1.3     12.09.2022
 - transition to PySide2+Python3

1.2     25.11.2021
 - fixed unused block is counted as error
 - fixed reported number of bad blocks with "-i errors"
 - added track/sector tracking in GUI
 - some python3 and Qt fixes

1.1     21.08.2015
 - added check for double-sided image (byte 3 on 18:00)
 - changed prgcall text for windows
 - added info option that prints various informations about d64 file
 - fixed error with whitespace stripping
 - fixed error with indexing of different correct blocks
 - added information about error code for different blocks
 - added rechecking of different blocks in GUI after image (de)selection
 - wider set of chars for normname stripping
 - added check for more than 35 tracks (bytes AC and C0 on 18:00)
 - added tooltips for image names with basic image information
 - image name is colored red if there is maybe more than 35 tracks or if the image is maybe double-sided
 - status bar shows if there is maybe more than 35 tracks or if the image is maybe double-sided
 - when combining images, name and normname are taken from first image with correct 18:0 track

1.0 - Initial version
"""

